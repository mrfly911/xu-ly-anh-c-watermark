using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Text;
using System.Windows.Forms;
// thu vien XU LY ANH
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;

namespace XuLyAnh_01
{
    public partial class FormMain : Form
    {
        public String pathImage; // duong dan chua hinh
        ToolTip t = new ToolTip();


        public FormMain()
        {
            InitializeComponent();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            openFileDialog1.Title = " Open Image...";
            openFileDialog1.InitialDirectory = "";
            openFileDialog1.Filter = " Image file (*.BMP,*.JPG,*.JPEG,*GIF,*.PNG)|*.bmp;*.jpg;*.jpeg;*.gif;*.png ";
            openFileDialog1.FileName = "";

            if (DialogResult.OK == openFileDialog1.ShowDialog())
            {
                pathImage = openFileDialog1.FileName;
                FormImage fImage = new FormImage(new Bitmap(pathImage));
                fImage.MdiParent = this;
                fImage.Show();


            }
        }

        private void FormMain_Load(object sender, EventArgs e)
        {

        }

        private void btnWM_Click(object sender, EventArgs e)
        {
            try
            {

                // lấy hình từ FormImage cho vào biến bmp
                FormImage currentform = (FormImage)this.ActiveMdiChild;
                Bitmap bmp = new Bitmap(currentform.bmp);

                // lấy Font
                string font = this.cboFont.Text;
                MessageBox.Show(font);

                // lấy size chữ
                int size = int.Parse(txtSize.Text);

                // lấy màu
                int mau = 3; // mặc định là white
                if (cboColor.Text == "Black")
                    mau = 1;
                else if (cboColor.Text == "Red")
                    mau = 2;


                // lấy vị trí
                int wmViTri = 3; // mặc định là ở dưới (bottom)
                if (cboViTri.Text == "Top")
                    wmViTri = 1;
                else if (cboViTri.Text == "Center")
                    wmViTri = 2;

                // lấy độ trong suốt
                int wmTran = int.Parse(txtTran.Text);

                // đánh dấu vào hình
                Bitmap bmpNew = waterMark(bmp, txtWM.Text, font, size, mau, wmTran, wmViTri);

                // Vẽ hình mới từ biến bmp
                FormImage newForm = new FormImage(bmpNew);
                newForm.MdiParent = this;
                newForm.Show();

            }
            catch (Exception ie)
            {
                MessageBox.Show(ie.ToString());
            }


        }

        private Bitmap waterMark(Bitmap bmPhoto, string txtWM, string wmFont, int wmSize, int wmMau, int wmTran, int wmViTri)
        {
            //create a image object containing the photograph to watermark
            Image imgPhoto = bmPhoto;
            int phWidth = imgPhoto.Width;
            int phHeight = imgPhoto.Height;

            //load the Bitmap into a Graphics object 
            Graphics grPhoto = Graphics.FromImage(bmPhoto);



            //------------------------------------------------------------
            //Step #1 - Insert Copyright message
            //------------------------------------------------------------

            //Set the rendering quality for this Graphics object
            grPhoto.SmoothingMode = SmoothingMode.AntiAlias;

            //Draws the photo Image object at original size to the graphics object.
            grPhoto.DrawImage(
                imgPhoto,                               // Photo Image object
                new Rectangle(0, 0, phWidth, phHeight), // Rectangle structure
                0,                                      // x-coordinate of the portion of the source image to draw. 
                0,                                      // y-coordinate of the portion of the source image to draw. 
                phWidth,                                // Width of the portion of the source image to draw. 
                phHeight,                               // Height of the portion of the source image to draw. 
                GraphicsUnit.Pixel);                    // Units of measure 

            //-------------------------------------------------------
            //to maximize the size of the Copyright message we will 
            //test multiple Font sizes to determine the largest posible 
            //font we can use for the width of the Photograph
            //define an array of point sizes you would like to consider as possiblities
            //-------------------------------------------------------

            Font crFont = null;
            SizeF crSize = new SizeF();


            //set a Font object to Arial (i)pt, Bold
            crFont = new Font(wmFont, wmSize, FontStyle.Bold);
            //Measure the Copyright string in this Font
            crSize = grPhoto.MeasureString(txtWM, crFont);

            //Since all photographs will have varying heights, determine a 
            //position 5% from the bottom of the image
            int yPixlesFromBottom = (int)(phHeight * .05); // mặc định là ở dưới ảnh
            if (wmViTri == 1)
                yPixlesFromBottom = (int)(phHeight * .95);

            else if (wmViTri == 2)
                yPixlesFromBottom = (int)(phHeight * .5);


            //Now that we have a point size use the Copyrights string height 
            //to determine a y-coordinate to draw the string of the photograph
            float yPosFromBottom = ((phHeight - yPixlesFromBottom) - (crSize.Height / 2));

            //Determine its x-coordinate by calculating the center of the width of the image
            float xCenterOfImg = (phWidth / 2);

            //Define the text layout by setting the text alignment to centered
            StringFormat StrFormat = new StringFormat();
            StrFormat.Alignment = StringAlignment.Center;


            // tạo bóng cho chữ
            //define a Brush which is semi trasparent black
            // độ trong suốt (Alpha set to 153)
            SolidBrush semiTransBrush2 = new SolidBrush(Color.FromArgb(wmTran, 0, 0, 0));

            //Draw the Copyright string
            grPhoto.DrawString(txtWM,                 //string of text
                crFont,                                   //font
                semiTransBrush2,                           //Brush
                new PointF(xCenterOfImg + 1, yPosFromBottom + 1),  //Position
                StrFormat);


            // chữ chính

            // mặc định là màu trắng
            //define a Brush which is semi trasparent white 
            // độ trong suốt (Alpha set to 153)
            SolidBrush semiTransBrush = new SolidBrush(Color.FromArgb(wmTran, 255, 255, 255));

            // màu đen
            if (wmMau == 1)
                semiTransBrush = new SolidBrush(Color.FromArgb(wmTran, 0, 0, 0));
            else
                // màu đỏ
                if (wmMau == 2)
                    semiTransBrush = new SolidBrush(Color.FromArgb(wmTran, 255, 0, 0));



            // Vẽ lại chữ trên bóng màu đen
            //Draw the Copyright string a second time to create a shadow effect
            //Make sure to move this text 1 pixel to the right and down 1 pixel

            grPhoto.DrawString(txtWM,                 //string of text
                crFont,                                   //font
                semiTransBrush,                           //Brush
                new PointF(xCenterOfImg, yPosFromBottom),  //Position
                StrFormat);                               //Text alignment


            //Replace the original photgraphs bitmap with the new Bitmap
            // imgPhoto = bmPhoto;
            grPhoto.Dispose();


            return bmPhoto;


        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {


            saveFileDialog1.Title = " Save Image...";
            saveFileDialog1.Filter = " JPeg Image|*.jpg|Gif Image|*.gif|PNG Image|*.png|Bitmap Image|*.bmp ";
            saveFileDialog1.OverwritePrompt = true;
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                // lấy hình từ FormImage cho vào biến bmp
                FormImage currentform = (FormImage)this.ActiveMdiChild;
                Bitmap bmp = new Bitmap(currentform.bmp);
                bmp.Save(saveFileDialog1.FileName);
            }

        }

        private void cboViTri_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = true;
        }

        private void txtTran_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsDigit(e.KeyChar) && !Char.IsControl(e.KeyChar))
            {
                t.ToolTipIcon = ToolTipIcon.Error;
                t.ToolTipTitle = "Lỗi";
                t.Show("Chỉ cho phép nhập số !", txtTran, 1000);
                e.Handled = true;
            }
        }

        private void txtSize_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsDigit(e.KeyChar) && !Char.IsControl(e.KeyChar))
            {
                t.ToolTipIcon = ToolTipIcon.Error;
                t.ToolTipTitle = "Lỗi";
                t.Show("Chỉ cho phép nhập số !", txtSize, 1000);
                e.Handled = true;
            }
        }

        private void cboColor_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = true;
        }

        private void cboFont_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = true;
        }


    }
}
