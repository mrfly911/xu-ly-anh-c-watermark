namespace XuLyAnh_01
{
    partial class FormMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
this.menuStrip1 = new System.Windows.Forms.MenuStrip();
this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
this.txtWM = new System.Windows.Forms.TextBox();
this.btnWM = new System.Windows.Forms.Button();
this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
this.cboFont = new System.Windows.Forms.ComboBox();
this.cboViTri = new System.Windows.Forms.ComboBox();
this.txtSize = new System.Windows.Forms.TextBox();
this.cboColor = new System.Windows.Forms.ComboBox();
this.lblText = new System.Windows.Forms.Label();
this.txtTran = new System.Windows.Forms.TextBox();
this.lblDoTrongSuot = new System.Windows.Forms.Label();
this.menuStrip1.SuspendLayout();
this.SuspendLayout();
// 
// menuStrip1
// 
this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
this.menuStrip1.Location = new System.Drawing.Point(0, 0);
this.menuStrip1.Name = "menuStrip1";
this.menuStrip1.Size = new System.Drawing.Size(1071, 24);
this.menuStrip1.TabIndex = 1;
this.menuStrip1.Text = "menuStrip1";
// 
// fileToolStripMenuItem
// 
this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openToolStripMenuItem,
            this.saveToolStripMenuItem,
            this.exitToolStripMenuItem});
this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
this.fileToolStripMenuItem.Size = new System.Drawing.Size(35, 20);
this.fileToolStripMenuItem.Text = "File";
// 
// openToolStripMenuItem
// 
this.openToolStripMenuItem.Name = "openToolStripMenuItem";
this.openToolStripMenuItem.Size = new System.Drawing.Size(111, 22);
this.openToolStripMenuItem.Text = "Open";
this.openToolStripMenuItem.Click += new System.EventHandler(this.openToolStripMenuItem_Click);
// 
// saveToolStripMenuItem
// 
this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
this.saveToolStripMenuItem.Size = new System.Drawing.Size(111, 22);
this.saveToolStripMenuItem.Text = "Save";
this.saveToolStripMenuItem.Click += new System.EventHandler(this.saveToolStripMenuItem_Click);
// 
// exitToolStripMenuItem
// 
this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
this.exitToolStripMenuItem.Size = new System.Drawing.Size(111, 22);
this.exitToolStripMenuItem.Text = "Exit";
this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
// 
// openFileDialog1
// 
this.openFileDialog1.FileName = "openFileDialog1";
// 
// txtWM
// 
this.txtWM.Location = new System.Drawing.Point(95, 3);
this.txtWM.Name = "txtWM";
this.txtWM.Size = new System.Drawing.Size(288, 20);
this.txtWM.TabIndex = 2;
// 
// btnWM
// 
this.btnWM.Location = new System.Drawing.Point(927, 0);
this.btnWM.Name = "btnWM";
this.btnWM.Size = new System.Drawing.Size(75, 23);
this.btnWM.TabIndex = 7;
this.btnWM.Text = "WaterMark";
this.btnWM.UseVisualStyleBackColor = true;
this.btnWM.Click += new System.EventHandler(this.btnWM_Click);
// 
// cboFont
// 
this.cboFont.FormattingEnabled = true;
this.cboFont.Items.AddRange(new object[] {
            "Arial",
            "Times New Roman",
            "Verdana",
            "Courier New"});
this.cboFont.Location = new System.Drawing.Point(391, 2);
this.cboFont.Name = "cboFont";
this.cboFont.Size = new System.Drawing.Size(128, 21);
this.cboFont.TabIndex = 3;
this.cboFont.Text = "Arial";
this.cboFont.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cboFont_KeyPress);
// 
// cboViTri
// 
this.cboViTri.FormattingEnabled = true;
this.cboViTri.Items.AddRange(new object[] {
            "Top",
            "Center",
            "Bottom"});
this.cboViTri.Location = new System.Drawing.Point(835, 4);
this.cboViTri.Name = "cboViTri";
this.cboViTri.Size = new System.Drawing.Size(69, 21);
this.cboViTri.TabIndex = 6;
this.cboViTri.Text = "Bottom";
this.cboViTri.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cboViTri_KeyPress);
// 
// txtSize
// 
this.txtSize.Location = new System.Drawing.Point(525, 4);
this.txtSize.Name = "txtSize";
this.txtSize.Size = new System.Drawing.Size(54, 20);
this.txtSize.TabIndex = 4;
this.txtSize.Text = "25";
this.txtSize.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtSize_KeyPress);
// 
// cboColor
// 
this.cboColor.FormattingEnabled = true;
this.cboColor.Items.AddRange(new object[] {
            "White",
            "Black",
            "Red"});
this.cboColor.Location = new System.Drawing.Point(585, 4);
this.cboColor.Name = "cboColor";
this.cboColor.Size = new System.Drawing.Size(75, 21);
this.cboColor.TabIndex = 5;
this.cboColor.Text = "White";
this.cboColor.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cboColor_KeyPress);
// 
// lblText
// 
this.lblText.AutoSize = true;
this.lblText.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
this.lblText.Location = new System.Drawing.Point(60, 6);
this.lblText.Name = "lblText";
this.lblText.Size = new System.Drawing.Size(31, 13);
this.lblText.TabIndex = 10;
this.lblText.Text = "Text:";
// 
// txtTran
// 
this.txtTran.Location = new System.Drawing.Point(766, 2);
this.txtTran.Name = "txtTran";
this.txtTran.Size = new System.Drawing.Size(42, 20);
this.txtTran.TabIndex = 11;
this.txtTran.Text = "153";
this.txtTran.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTran_KeyPress);
// 
// lblDoTrongSuot
// 
this.lblDoTrongSuot.AutoSize = true;
this.lblDoTrongSuot.Location = new System.Drawing.Point(679, 5);
this.lblDoTrongSuot.Name = "lblDoTrongSuot";
this.lblDoTrongSuot.Size = new System.Drawing.Size(40, 13);
this.lblDoTrongSuot.TabIndex = 12;
this.lblDoTrongSuot.Text = "Optical";
// 
// FormMain
// 
this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
this.ClientSize = new System.Drawing.Size(1071, 392);
this.Controls.Add(this.lblDoTrongSuot);
this.Controls.Add(this.txtTran);
this.Controls.Add(this.lblText);
this.Controls.Add(this.cboColor);
this.Controls.Add(this.txtSize);
this.Controls.Add(this.cboViTri);
this.Controls.Add(this.cboFont);
this.Controls.Add(this.btnWM);
this.Controls.Add(this.txtWM);
this.Controls.Add(this.menuStrip1);
this.IsMdiContainer = true;
this.MainMenuStrip = this.menuStrip1;
this.Name = "FormMain";
this.Text = "Image Processing - WaterMark";
this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
this.Load += new System.EventHandler(this.FormMain_Load);
this.menuStrip1.ResumeLayout(false);
this.menuStrip1.PerformLayout();
this.ResumeLayout(false);
this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.TextBox txtWM;
        private System.Windows.Forms.Button btnWM;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.ComboBox cboFont;
        private System.Windows.Forms.ComboBox cboViTri;
        private System.Windows.Forms.TextBox txtSize;
        private System.Windows.Forms.ComboBox cboColor;
        private System.Windows.Forms.Label lblText;
        private System.Windows.Forms.TextBox txtTran;
        private System.Windows.Forms.Label lblDoTrongSuot;
    }
}

