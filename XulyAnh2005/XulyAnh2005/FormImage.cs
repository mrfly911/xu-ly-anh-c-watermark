﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace XuLyAnh_01
{
    public partial class FormImage : Form
    {
        //public FormImage()
        //{
        //    InitializeComponent();
        //}
        public Bitmap bmp;
        public FormImage(Bitmap _bmp)
        {
            InitializeComponent();
            bmp = new Bitmap(_bmp);

        }

        private void FormImage_Paint_1(object sender, PaintEventArgs e)
        {
            Graphics g = e.Graphics;
            g.DrawImage(new Bitmap(bmp), this.ClientRectangle);
        }

        private void FormImage_Resize(object sender, EventArgs e)
        {
            Invalidate();
        }

        private void FormImage_Load(object sender, EventArgs e)
        {

        }




    }
}
