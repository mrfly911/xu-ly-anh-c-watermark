using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Text;
using System.Windows.Forms;
// NEW LIB
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;

namespace WaterMark
{
    public partial class FormMain : Form
    {
        public String pathImage; // Save the img path

        public FormMain()
        {
            InitializeComponent();
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            openFileDialog1.Title = " Open Image...";
            openFileDialog1.InitialDirectory = "";
            openFileDialog1.Filter = " Image file (*.BMP,*.JPG,*.JPEG,*GIF,*.PNG)|*.bmp;*.jpg;*.jpeg;*.gif;*.png ";
            openFileDialog1.FileName = "";

            if (DialogResult.OK == openFileDialog1.ShowDialog())
            {
                pathImage = openFileDialog1.FileName;
                // Do later
                FormImg fImage = new FormImg(new Bitmap(pathImage));
                fImage.MdiParent = this;
                fImage.Show();
  
            }
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            saveFileDialog1.Title = " Save Image...";
            saveFileDialog1.Filter = " JPeg Image|*.jpg|Gif Image|*.gif|PNG Image|*.png|Bitmap Image|*.bmp ";
            saveFileDialog1.OverwritePrompt = true;
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                // Do later
                FormImg currentform = (FormImg)this.ActiveMdiChild;
                Bitmap bmp = new Bitmap(currentform.bmp);
                bmp.Save(saveFileDialog1.FileName);
            }
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            try
            {

                // get image from FormImg to bmp
                FormImg currentform = (FormImg)this.ActiveMdiChild;
                Bitmap bmp = new Bitmap(currentform.bmp);

                // get Font
                string font = "Arial";
                if (this.cbFont.Text != "Font")
                    font = this.cbFont.Text;
                

                // get font size
                int size = 19;
                if (this.txtFontSize.Text != "Size")
                    size = int.Parse(txtFontSize.Text);
                

                // get color
                int color = 3; //  white is default
                if (cbColor.Text == "Black")
                    color = 1;
                else if (cbColor.Text == "Red")
                    color = 2;
                

                // get position
                int wmPositon = 3; // bottom is default
                if (cbPosition.Text == "Top")
                    wmPositon = 1;
                else if (cbPosition.Text == "Middle")
                    wmPositon = 2;

                // get optical
                int wmOptical = 255; // bottom is default
                if (cbOptical.Text == "50%")
                    wmOptical = 127;
                else if (cbOptical.Text == "10%")
                    wmOptical = 25;

                // set the watermark
                Bitmap bmpNew = waterMark(bmp, txtText.Text, font, size, color, wmOptical, wmPositon);

                // draw new image from bmp to FormImg
                FormImg newForm = new FormImg(bmpNew);
                newForm.MdiParent = this;
                newForm.Show();

            }
            catch (Exception ie)
            {
                MessageBox.Show(ie.ToString());
            }

        }

        private Bitmap waterMark(Bitmap bmPhoto, string txtWM, string wmFont, int wmSize, int wmMau, int wmTran, int wmViTri)
        {
            //create a image object containing the photograph to watermark
            Image imgPhoto = bmPhoto;
            int phWidth = imgPhoto.Width;
            int phHeight = imgPhoto.Height;

            //load the Bitmap into a Graphics object 
            Graphics grPhoto = Graphics.FromImage(bmPhoto);



            //------------------------------------------------------------
            //Step #1 - Insert Copyright message
            //------------------------------------------------------------

            //Set the rendering quality for this Graphics object
            grPhoto.SmoothingMode = SmoothingMode.AntiAlias;

            //Draws the photo Image object at original size to the graphics object.
            grPhoto.DrawImage(
                imgPhoto,                               // Photo Image object
                new Rectangle(0, 0, phWidth, phHeight), // Rectangle structure
                0,                                      // x-coordinate of the portion of the source image to draw. 
                0,                                      // y-coordinate of the portion of the source image to draw. 
                phWidth,                                // Width of the portion of the source image to draw. 
                phHeight,                               // Height of the portion of the source image to draw. 
                GraphicsUnit.Pixel);                    // Units of measure 

            //-------------------------------------------------------
            //to maximize the size of the Copyright message we will 
            //test multiple Font sizes to determine the largest possible 
            //font we can use for the width of the Photograph
            //define an array of point sizes you would like to consider as possibilities
            //-------------------------------------------------------

            Font crFont = null;
            SizeF crSize = new SizeF();


            //set a Font object to Arial (i)pt, Bold
            crFont = new Font(wmFont, wmSize, FontStyle.Bold);
            //Measure the Copyright string in this Font
            crSize = grPhoto.MeasureString(txtWM, crFont);

            //Since all photographs will have varying heights, determine a 
            //position 5% from the bottom of the image
            int yPixlesFromBottom = (int)(phHeight * .05); // defaul is bottom
            if (wmViTri == 1)
                yPixlesFromBottom = (int)(phHeight * .95);

            else if (wmViTri == 2)
                yPixlesFromBottom = (int)(phHeight * .5);


            //Now that we have a point size use the Copyrights string height 
            //to determine a y-coordinate to draw the string of the photograph
            float yPosFromBottom = ((phHeight - yPixlesFromBottom) - (crSize.Height / 2));

            //Determine its x-coordinate by calculating the center of the width of the image
            float xCenterOfImg = (phWidth / 2);

            //Define the text layout by setting the text alignment to centered
            StringFormat StrFormat = new StringFormat();
            StrFormat.Alignment = StringAlignment.Center;


            // create shadow
            //define a Brush which is semi trasparent black
            // set Optical (Alpha set to 153)
            SolidBrush semiTransBrush2 = new SolidBrush(Color.FromArgb(wmTran, 0, 0, 0));

            //Draw the Copyright string
            grPhoto.DrawString(txtWM,                 //string of text
                crFont,                                   //font
                semiTransBrush2,                           //Brush
                new PointF(xCenterOfImg + 1, yPosFromBottom + 1),  //Position
                StrFormat);


            // Main TEXT

            // white is default color
            //define a Brush which is semi transparent white 
            // set Optical (Alpha set to 153)
            SolidBrush semiTransBrush = new SolidBrush(Color.FromArgb(wmTran, 255, 255, 255));

            // black
            if (wmMau == 1)
                semiTransBrush = new SolidBrush(Color.FromArgb(wmTran, 0, 0, 0));
            else
                // red
                if (wmMau == 2)
                    semiTransBrush = new SolidBrush(Color.FromArgb(wmTran, 255, 0, 0));



            // draw TEXT again on a black background
            //Draw the Copyright string a second time to create a shadow effect
            //Make sure to move this text 1 pixel to the right and down 1 pixel

            grPhoto.DrawString(txtWM,                 //string of text
                crFont,                                   //font
                semiTransBrush,                           //Brush
                new PointF(xCenterOfImg, yPosFromBottom),  //Position
                StrFormat);                               //Text alignment


            //Replace the original photographs bitmap with the new Bitmap
            // imgPhoto = bmPhoto;
            grPhoto.Dispose();


            return bmPhoto;


        }

        private void FormMain_Load(object sender, EventArgs e)
        {

        }

        
    }
}