using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace WaterMark
{
    public partial class FormImg : Form
    {
        public Bitmap bmp;

        public FormImg(Bitmap _bmp)
        {
            InitializeComponent();

            bmp = new Bitmap(_bmp);
        }

        private void FormImg_Paint(object sender, PaintEventArgs e)
        {
            Graphics g = e.Graphics;
            g.DrawImage(new Bitmap(bmp), this.ClientRectangle);
        }

        private void FormImg_Resize(object sender, EventArgs e)
        {
            Invalidate();
        }
    }
}